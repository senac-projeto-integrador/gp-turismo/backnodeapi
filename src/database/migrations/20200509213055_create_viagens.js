
exports.up = function(knex) {
  return knex.schema.createTable('viagens', function(table){
    table.increments();

    table.string('horaPartida').notNullable();
    table.string('horaChegada').notNullable();
    table.decimal('valor').notNullable();
    table.string('tipoViagem').notNullable();
    table.string('cidadePartida').notNullable();
    table.string('cidadeChegada').notNullable();
    
    table.string('excursao_id').notNullable();
    table.foreign('excursao_id').references('id').inTable('excursoes');
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('viagens');
};
