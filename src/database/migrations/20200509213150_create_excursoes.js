
exports.up = function(knex) {
  return knex.schema.createTable('excursoes', function(table){
    table.string('id').primary();
    table.string('code').notNullable();
    table.string('dataPartida').notNullable();
    table.string('dataChegada').notNullable();
    table.string('detalhesExcursoes').notNullable();
  })
};

exports.down = function(knex) {
  return knex.schema.dropTable('excursoes')
};
