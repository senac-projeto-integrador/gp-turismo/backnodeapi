const express = require('express');
const routes = require('./routes');
const cors = require('cors');
const { uuidv4 } = require('uuidv4');
const app = express();

app.use(cors());
app.use(express.json());
app.use(routes);

module.exports = app;